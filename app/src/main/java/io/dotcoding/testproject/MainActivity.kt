package io.dotcoding.testproject

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val pagerAdapter = PagerAdapter(this, supportFragmentManager)
        pagerAdapter.addFragment(BlankFragment(), "title")
        pagerAdapter.addFragment(BlankFragment(), "title")
        pagerAdapter.addFragment(BlankFragment(), "title")
        pagerAdapter.addFragment(BlankFragment(), "title")
        viewPager.adapter = pagerAdapter

        buttonsLayout.setupViewPager(viewPager)
    }
}

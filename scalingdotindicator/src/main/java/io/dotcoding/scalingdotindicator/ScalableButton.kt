package io.dotcoding.scalingdotindicator

import android.animation.AnimatorSet
import android.animation.PropertyValuesHolder
import android.animation.ValueAnimator
import android.annotation.TargetApi
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF
import android.os.Build
import android.support.annotation.ColorInt
import android.support.annotation.DrawableRes
import android.util.AttributeSet
import android.util.Pair
import android.view.animation.DecelerateInterpolator
import android.widget.LinearLayout
import io.dotcoding.scalingdotindicator.styles.FigaroBtnStyleKit

/**
 * Created by Eduard Albu on 29 Июнь 2018.
 * For project: TestProject
 * Copyright (c) 2018. .coding (dotcoding.io)
 */
class ScalableImageButton : LinearLayout {

    private lateinit var originalFrame: RectF
    private lateinit var maxSize: Pair<Float, Float>
    private lateinit var minSize: Pair<Float, Float>
    private lateinit var styleKit: StyleKit
    var isBig = false
    var delegate: ButtonDelegate? = null
    private var buttonHeight: Int = 0
    private var buttonWidth: Int = 0
    private var buttonMinHeight: Int = 0
    private var buttonMinWidth: Int = 0
    private var shadowRadius: Int = 0
    private lateinit var paint: Paint

    @DrawableRes
    private var bgDrawable: Int = android.R.drawable.btn_plus

    @ColorInt
    private var bgColor: Int = resources.getColor(android.R.color.transparent)

    @ColorInt
    private var shadowColor: Int = resources.getColor(android.R.color.transparent)

    @DrawableRes
    private var btnIcon: Int = android.R.drawable.ic_media_next

    constructor(context: Context?, height: Int, width: Int, minHeight: Int, minWidth: Int, @ColorInt shadowColor: Int,
                @ColorInt bgColor: Int, @DrawableRes bgDrawable: Int,
                shadowRadius: Int, @DrawableRes btnIcon: Int) : super(context) {
        this.buttonHeight = height
        this.buttonWidth = width
        this.buttonMinHeight = minHeight
        this.buttonMinWidth = minWidth
        this.shadowRadius = shadowRadius
        this.bgDrawable = bgDrawable
        this.bgColor = bgColor
        this.shadowColor = shadowColor
        this.styleKit = FigaroBtnStyleKit()
        this.maxSize = Pair(buttonHeight.toFloat(), buttonWidth.toFloat())
        this.minSize = Pair(buttonMinHeight.toFloat(), buttonMinWidth.toFloat())
        this.originalFrame = RectF(0f, 0f, this.buttonMinWidth.toFloat(), this.buttonMinHeight.toFloat())
        paint = Paint(Paint.ANTI_ALIAS_FLAG)
        paint.strokeWidth = 5f
        paint.color = resources.getColor(android.R.color.white)
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        getAttributes(context!!, attrs)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        getAttributes(context!!, attrs)
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        getAttributes(context!!, attrs)
    }

    private fun getAttributes(context: Context, attrs: AttributeSet?) {
        if (attrs == null) return
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.ScalableImageButton, 0, 0)
        this.buttonHeight = typedArray.getDimensionPixelSize(R.styleable.ScalableImageButton_button_max_height, 0)
        this.buttonWidth = typedArray.getDimensionPixelSize(R.styleable.ScalableImageButton_button_max_width, 0)
        this.buttonMinHeight = typedArray.getDimensionPixelSize(R.styleable.ScalableImageButton_button_min_height, 0)
        this.buttonMinWidth = typedArray.getDimensionPixelSize(R.styleable.ScalableImageButton_button_min_width, 0)
        this.bgColor = typedArray.getColor(R.styleable.ScalableImageButton_button_background_color, resources.getColor(android.R.color.transparent))
        this.shadowColor = typedArray.getColor(R.styleable.ScalableImageButton_shadow_color, resources.getColor(android.R.color.transparent))
        this.bgDrawable = typedArray.getResourceId(R.styleable.ScalableImageButton_button_background_drawable, android.R.drawable.btn_plus)
        this.shadowRadius = typedArray.getDimensionPixelSize(R.styleable.ScalableImageButton_shadow_radius, 0)
        this.btnIcon = typedArray.getResourceId(R.styleable.ScalableImageButton_button_icon, android.R.drawable.ic_media_next)
        typedArray.recycle()
        this.styleKit = FigaroBtnStyleKit()
        this.maxSize = Pair(buttonHeight.toFloat(), buttonWidth.toFloat())
        this.minSize = Pair(buttonMinHeight.toFloat(), buttonMinWidth.toFloat())
        this.originalFrame = RectF(0f, 0f, this.buttonMinWidth.toFloat(), this.buttonMinHeight.toFloat())
        paint = Paint(Paint.ANTI_ALIAS_FLAG)
        paint.strokeWidth = 5f
        paint.color = resources.getColor(android.R.color.white)
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        val params = this.layoutParams
        params.width = buttonMinWidth
        params.height = (buttonMinHeight + (100 * 0.1)).toInt()
        this.layoutParams = params
    }

    override fun dispatchDraw(canvas: Canvas?) {
        this.setBackgroundColor(resources.getColor(android.R.color.transparent))
        styleKit.drawCanvas1(canvas, originalFrame, StyleKit.ResizingBehavior.Stretch, (originalFrame.height()/2))
        this.setOnClickListener { delegate?.didClickOnButton(this.tag as Int) }
        super.dispatchDraw(canvas)
    }

    fun setStyleKit(styleKit: StyleKit) {
        this.styleKit = styleKit
        this.invalidate()
    }

    fun toggleResize(increase: Boolean) {
        if (increase != this.isBig) {
            animateToSize()
            this.isBig = increase
        }
    }

    private fun animateToSize() {
        val canvasAnimator = ValueAnimator()
        val layoutAnimator = ValueAnimator()
        val widthHolder = PropertyValuesHolder.ofFloat("width",
                if (isBig) this.maxSize.second else this.minSize.second,
                if (isBig) this.minSize.second else this.maxSize.second
        )
        val heightHolder = PropertyValuesHolder.ofFloat("height",
                if (isBig) this.maxSize.first else this.minSize.first,
                if (isBig) this.minSize.first else this.maxSize.first
        )
        canvasAnimator.setValues(widthHolder, heightHolder)
        canvasAnimator.addUpdateListener { animator ->
            val width = animator.getAnimatedValue("width") as Float
            val height = animator.getAnimatedValue("height") as Float
            this@ScalableImageButton.originalFrame = RectF(0f, 0f, width, height)
            this@ScalableImageButton.invalidate()
        }

        layoutAnimator.setValues(widthHolder, heightHolder)
        layoutAnimator.addUpdateListener { animator ->
            val lParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            val width = animator.getAnimatedValue("width") as Float
            val height = animator.getAnimatedValue("height") as Float
            lParams.width = width.toInt()
            lParams.height = (height + (100 * 0.1)).toInt()
            this@ScalableImageButton.layoutParams = lParams
        }

        val animationSet = AnimatorSet()
        animationSet.interpolator = DecelerateInterpolator()
        animationSet.duration = 300
        animationSet.playTogether(canvasAnimator, layoutAnimator)
        animationSet.start()
    }

    interface ButtonDelegate {
        fun didClickOnButton(withIndex: Int)
    }
}
package io.dotcoding.scalingdotindicator

import android.util.DisplayMetrics
import android.util.TypedValue

/**
 * Created by Eduard Albu on 30 Июнь 2018.
 * For project: TestProject
 * Copyright (c) 2018. .coding (dotcoding.io)
 */
fun Int.toPx(displayMetrics: DisplayMetrics): Int {
    return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, this.toFloat(), displayMetrics).toInt()
}
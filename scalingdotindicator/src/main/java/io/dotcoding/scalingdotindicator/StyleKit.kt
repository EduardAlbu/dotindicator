package io.dotcoding.scalingdotindicator

import android.graphics.Canvas
import android.graphics.RectF
import io.dotcoding.scalingdotindicator.styles.FigaroBtnStyleKit

/**
 * Created by Eduard Albu on 01 Июль 2018.
 * For project: TestProject
 * Copyright (c) 2018. .coding (dotcoding.io)
 */
abstract class StyleKit {
    abstract fun drawCanvas1(canvas: Canvas?, cornerRadius: Float)
    abstract fun drawCanvas1(canvas: Canvas?, targetFrame: RectF, resizing: ResizingBehavior, cornerRadius: Float)

    // Resizing Behavior
    enum class ResizingBehavior {
        AspectFit, //!< The content is proportionally resized to fit into the target rectangle.
        AspectFill, //!< The content is proportionally resized to completely fill the target rectangle.
        Stretch, //!< The content is stretched to match the entire target rectangle.
        Center
        //!< The content is centered in the target rectangle, but it is NOT resized.
    }
}
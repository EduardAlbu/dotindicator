package io.dotcoding.scalingdotindicator

import android.annotation.TargetApi
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.media.Image
import android.os.Build
import android.support.annotation.ColorInt
import android.support.annotation.DrawableRes
import android.support.v4.view.ViewPager
import android.util.AttributeSet
import android.view.Gravity
import android.widget.ImageView
import android.widget.LinearLayout

/**
 * Created by Eduard Albu on 29 Июнь 2018.
 * For project: TestProject
 * Copyright (c) 2018. Fabity.co
 */
class ButtonsLayout : LinearLayout, ViewPager.OnPageChangeListener, ScalableImageButton.ButtonDelegate {

    private lateinit var viewPager: ViewPager
    private var buttons: MutableList<ScalableImageButton> = mutableListOf<ScalableImageButton>()

    private var buttonHeight: Int = 0
    private var buttonWidth: Int = 0
    private var buttonMinHeight: Int = 0
    private var buttonMinWidth: Int = 0
    private var shadowRadius: Int = 0

    @DrawableRes
    private var bgDrawable: Int = android.R.drawable.btn_plus

    @ColorInt
    private var bgColor: Int = resources.getColor(android.R.color.transparent)

    @ColorInt
    private var shadowColor: Int = resources.getColor(android.R.color.transparent)

    @DrawableRes
    private var btnIcon: Int = android.R.drawable.ic_media_next

    private lateinit var attrs: AttributeSet

    constructor(context: Context?) : super(context)

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        getAttributes(context!!, attrs)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        getAttributes(context!!, attrs)
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        getAttributes(context!!, attrs)
    }

    private fun getAttributes(context: Context, attrs: AttributeSet?) {
        if (attrs == null) return
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.ScalableImageButton, 0, 0)
        this.buttonHeight = typedArray.getDimensionPixelSize(R.styleable.ScalableImageButton_button_max_height, 0)
        this.buttonWidth = typedArray.getDimensionPixelSize(R.styleable.ScalableImageButton_button_max_width, 0)
        this.buttonMinHeight = typedArray.getDimensionPixelSize(R.styleable.ScalableImageButton_button_min_height, 0)
        this.buttonMinWidth = typedArray.getDimensionPixelSize(R.styleable.ScalableImageButton_button_min_width, 0)
        this.bgColor = typedArray.getColor(R.styleable.ScalableImageButton_button_background_color, resources.getColor(android.R.color.transparent))
        this.shadowColor = typedArray.getColor(R.styleable.ScalableImageButton_shadow_color, resources.getColor(android.R.color.transparent))
        this.bgDrawable = typedArray.getResourceId(R.styleable.ScalableImageButton_button_background_drawable, android.R.drawable.btn_plus)
        this.shadowRadius = typedArray.getDimensionPixelSize(R.styleable.ScalableImageButton_shadow_radius, 0)
        this.btnIcon = typedArray.getResourceId(R.styleable.ScalableImageButton_button_icon, android.R.drawable.ic_media_next)
        typedArray.recycle()
    }

    fun setupViewPager(viewPager: ViewPager) {
        this.viewPager = viewPager
        this.viewPager.addOnPageChangeListener(this)
        createButtons()
    }

    private fun createButtons() {
        this.buttons.clear()
        val adapter = viewPager.adapter
        if (adapter != null) {
            for (i in 0 until adapter.count) {
                val button = ScalableImageButton(context, buttonHeight, buttonWidth,
                        buttonMinHeight, buttonMinWidth, shadowColor, bgColor, bgDrawable,
                        shadowRadius, btnIcon)
                button.tag = i
                button.delegate = this
                this.buttons.add(button)

                this.addView(button)
                button.toggleResize(i == 0)
                val params = button.layoutParams as LinearLayout.LayoutParams
                params.gravity = Gravity.CENTER
                params.setMargins(0, 0, 12.toPx(resources.displayMetrics), 0)
                button.layoutParams = params

                val imageView = ImageView(context)
                imageView.setImageResource(this.btnIcon)
                imageView.scaleType = ImageView.ScaleType.CENTER_INSIDE
                button.addView(imageView)
                val imageViewParams = imageView.layoutParams as LinearLayout.LayoutParams
                imageViewParams.height = buttonHeight
                imageViewParams.width = buttonWidth
                imageView.setPadding(0, 0, 0, (buttonHeight * 0.1).toInt())
                imageView.layoutParams = imageViewParams
            }
        } else throw RuntimeException("ViewPager has no attached adapter")
        this.viewPager.currentItem = 0
    }

    override fun onPageScrollStateChanged(state: Int) {

    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

    }

    override fun onPageSelected(position: Int) {
        for (button in this.buttons) {
            button.toggleResize((button.tag as Int) == position)
        }
    }

    override fun didClickOnButton(withIndex: Int) {
        if ((withIndex + 1) != this.viewPager.adapter?.count)
            this.viewPager.setCurrentItem(withIndex + 1, true)
    }

    interface LayoutDelegate {
        fun didClickOnNext()
    }
}